import React, { Component } from 'react';
import CardList from './CardList';
import './Courses.css';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getCourses } from '../../actions/courseActions';


class Courses extends Component {

  constructor() {
    super();
    this.state={
      notMyCourses:[],
      myCourses:[],
      ali:[]
    };
  }

  componentDidMount() {
    const { user, isAuthenticated } = this.props.auth;
    const { courses } = this.props.courses;
    this.props.getCourses();
    if (isAuthenticated) {
      const { notMyCourses, myCourses } = this.state;
      for(var i in courses) {
        for(var j in user.cources) {
          if( courses[i]._id === user.cources[j] )
            myCourses.push(courses[i]);
          else
            notMyCourses.push(courses[i]);
        }
      }
    }
  }
  
  render() {
    return (
      <div className='teachersbg' >
        <CardList notMyCourses={this.state.myCourses} />
      </div>
    )
  }

};

Courses.propTypes = {
  getCourses: PropTypes.func.isRequired,
  courses: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  courses: state.courses,
  auth: state.auth
});

export default connect(mapStateToProps, { getCourses })(Courses);