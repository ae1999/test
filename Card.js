import React, { Component } from 'react';
import './Card.css';
import booksimg from '../img/books.png';


class Card extends Component {
	render() {
		const { id, name, teacher, details } = this.props;
		return (
			<div className='hi'>
				<div className='row'>
				
					<div className='col-md-3 pict'><img src={booksimg} alt=""/></div>
					<div className='col-md-9'>
						<h2 className='whitetext direction'>{id}</h2>
						<h2 className='whitetext direction'>{name}</h2>
						<h2 className='whitetext direction'>{teacher}</h2>
						<h2 className='whitetext direction'>{details}</h2>
						<button className='btn btn1N colorwhite center'>ثبت نام</button>
					</div>
				
				</div>
			</div>
		);
	}
}

export default Card;