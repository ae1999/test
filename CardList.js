import React, { Component } from 'react';
import  Card from './Card';


class CardList extends Component {
	render() {
		const { notMyCourses } = this.props;
		const coursesArray = notMyCourses.map((course, i)=>{
			return <Card key={i} id={notMyCourses[i]._id} teacher={notMyCourses[i].courseTeacher} name={notMyCourses[i].courseName} details={notMyCourses[i].courseDetails} />
		})
		
		return (
			<div>
				{coursesArray}
			</div>
		);
	}
}

export default CardList;

